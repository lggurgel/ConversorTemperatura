# ConversorTemperatura - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de converter temperatura nas unidades Celsius, Kelvin e Fahrenheit.

## Ordem das entradas:

1. Valor da temperatura que será convertida
2. A unidade dessa temperatura: 1, 2 ou 3.


### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

Abra o terminal;
Encontre o diretório raiz do projeto

Limpe os arquivos objeto: $ make clean

Compile o programa: $ make

Execute: $ make run
