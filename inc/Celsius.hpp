#ifndef CELSIUS_HPP
#define CELSIUS_HPP

#include "Temperatura.hpp"
#include <string>
#include <iostream>

using namespace std;

class Celsius : public Temperatura{

public:
	Celsius(float temperatura);
	Celsius();
	~Celsius();

	string conversorTemperatura();

};
#endif
