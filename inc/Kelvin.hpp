#ifndef KELVIN_HPP
#define KELVIN_HPP

#include "Temperatura.hpp"

class Kelvin : public Temperatura{

public:
  Kelvin();
  Kelvin(float temperatura);
  ~Kelvin();

  string conversorTemperatura();


};
#endif
