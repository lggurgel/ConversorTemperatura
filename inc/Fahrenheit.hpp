#ifndef FAHRENHEIT_HPP
#define FAHRENHEIT_HPP

#include "Temperatura.hpp"

class Fahrenheit : public Temperatura{

public:
  Fahrenheit();
  Fahrenheit(float temperatura);
  ~Fahrenheit();

  string conversorTemperatura();

};
#endif
