#ifndef TEMPERATURA_HPP
#define TEMPERATURA_HPP

#include <iostream>
#include <string>

using namespace std;

class Temperatura{
protected:
	float temperatura;

public:
	Temperatura();
	Temperatura(float temperatura);
	~Temperatura();

	float getTemperatura();
	void setTemperatura(float temperatura);

	string conversorTemperatura();


};
#endif
