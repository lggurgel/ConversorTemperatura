#include "../inc/Celsius.hpp"
#include "../inc/Kelvin.hpp"
#include "../inc/Fahrenheit.hpp"

int main(){

	int menu;
	float temperatura;

	cout << "Entre com a temperatura: ";
	cin >> temperatura;

	cout << endl;
	cout << "(1) Celsius  (2) Kelvin   (3) Fahrenheit" << endl;
	cout << "Unidade de medida: ";
	cin >> menu;
	cout << endl;

	switch (menu) {
		case 1:{
			Celsius objCelsius(temperatura);
			cout << objCelsius.conversorTemperatura() << endl;
			break;}

		case 2:{
			Kelvin objKelvin(temperatura);
			cout << objKelvin.conversorTemperatura() << endl;
			break;}

		case 3:{
			Fahrenheit objFahrenheit(temperatura);
			cout << objFahrenheit.conversorTemperatura() << endl;
			break;}

		default:
			cout << "Unidade não encontrada º" << endl;
			break;
	}

	cout << endl;

	return 0;
}
