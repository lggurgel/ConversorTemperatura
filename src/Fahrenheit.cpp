#include "../inc/Fahrenheit.hpp"

Fahrenheit::Fahrenheit(){}

Fahrenheit::Fahrenheit(float temperatura){
  this->temperatura = temperatura;
}

Fahrenheit::~Fahrenheit(){}

string Fahrenheit::conversorTemperatura(){

  double celsius = (this->temperatura - 32)*5/9;
  double kelvin = celsius + 273;

  string mensagem = "Temperatura em Celsius: " + to_string(celsius) + " Temperatura em Kelvin: " + to_string(kelvin);

  return mensagem;
}
