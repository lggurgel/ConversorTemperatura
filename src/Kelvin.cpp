#include "../inc/Kelvin.hpp"

Kelvin::Kelvin(){}

Kelvin::Kelvin(float temperatura){
  this->temperatura = temperatura;
}

Kelvin::~Kelvin(){}

string Kelvin::conversorTemperatura(){

  float celsius = this->temperatura - 273;
  float fahrenheit = (celsius * 9/5) + 32;

  string mensagem = "Temperatura em Celsius: " + to_string(celsius) + " Temperatura em Fahrenheit: " + to_string(fahrenheit);

  return mensagem;

}
